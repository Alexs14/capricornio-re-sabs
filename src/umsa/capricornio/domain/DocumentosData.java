/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package umsa.capricornio.domain;

/**
 *
 * @author Leo
 */
public class DocumentosData { 
    
    
    public String  cod_docs;
    public String  cod_transaccion;
    public String  terminos_ref;
    public String  ubicacion;
    public String descripcion;
    public String fecha_ini;
    public String fecha_fin;
    
    public DocumentosData(String cod_docs, String cod_transaccion,String terminos_ref,String ubicacion,String descripcion,String fecha_ini,String fecha_fin) {
        this.cod_docs=cod_docs;
        this.cod_transaccion=cod_transaccion;
        this.terminos_ref=terminos_ref;
        this.ubicacion=ubicacion;
        this.descripcion=descripcion;
        this.fecha_ini=fecha_ini;
        this.fecha_fin=fecha_fin;
    }
    
    public DocumentosData() {
        cod_docs="";
        cod_transaccion="";
        terminos_ref="";
        ubicacion="";
        descripcion="";
        fecha_ini="";
        fecha_fin="";
    }
}
