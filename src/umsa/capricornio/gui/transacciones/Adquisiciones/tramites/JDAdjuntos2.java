/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package umsa.capricornio.gui.transacciones.Adquisiciones.tramites;

import com.caucho.hessian.client.HessianProxyFactory;
import java.awt.Desktop;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.rmi.RemoteException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import javax.swing.Box;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.TableModelEvent;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.xml.rpc.ServiceException;
import net.sf.nachocalendar.components.DateField;
import umsa.capricornio.gui.ConnectADQUI.AdquiWSServiceLocator;
import umsa.capricornio.gui.ConnectADQUI.AdquiWS_PortType;
import umsa.capricornio.gui.menu.FrmMenu;
import umsa.capricornio.gui.transacciones.tablas.TablaDocumentos;
import umsa.capricornio.service.HelloService;
import umsa.capricornio.utilitarios.herramientas.Archivos;
import umsa.capricornio.utilitarios.herramientas.MiRenderer;

/**
 *
 * @author UMSA-JES
 */
public class JDAdjuntos2 extends javax.swing.JDialog {

    private File rutaArchivo;
    private String lectura;
    private String nombre_archivo;
    private int cod_trans_nro;
    private int cod_transaccion;
    TablaDocumentos documentos;
    FrmMenu menu;
    private int cod_rol;
    //private String origen;
    private Runtime r;
    private int cod_tipo_adj = -1;

    /**
     * Creates new form JDAdjuntos
     */
    public JDAdjuntos2(FrmMenu menu, boolean modal, int cod_transaccion, int cod_rol,int cod_trans_nro) {
        super(menu, modal);
        initComponents();
        /*this.rutaArchivo=rutaArchivo;
         this.lectura=lectura;
         this.nombre_archivo=nombre_archivo;*/
        this.cod_transaccion = cod_transaccion;
        this.cod_trans_nro=cod_trans_nro;
        //this.origen=origen;
        this.cod_rol = cod_rol;
        ConstruyeTablaDocumentos();
        this.LlenaDocumentos();
//        LlenaTipoAdj();
        //CheckOrigen(this.cod_rol);
    }

    /*private void CheckOrigen(int cod_rol) {
        System.out.println("El cod_rol es --> " + cod_rol);
        if (cod_rol == 7) {
            this.cod_tipo_adj = 9;
            this.JCB_TipoAdj.setSelectedIndex(9);
            this.JCB_TipoAdj.setEnabled(false);
        }
    }*/

    /*private void LlenaTipoAdj() {
        //this.BtnAdjuntar.setEnabled(false);
        try {
            AdquiWSServiceLocator servicio = new AdquiWSServiceLocator();
            AdquiWS_PortType puerto = servicio.getAdquiWS();
            Map[] datos = puerto.getTipoAdjuntos();
            if (datos != null) {
                for (int c = 0; c < datos.length; c++) {
                    //documentos.insert(c);
                    //TblDocumentos.tableChanged(new TableModelEvent(documentos, c, c, TableModelEvent.ALL_COLUMNS,TableModelEvent.INSERT));
                    //TblDocumentos.setValueAt(datos1[c].get("COD_DOCS"),c,0);
                    //ultimo_valor = (String) datos1[c].get("ULTIMO");
                    //System.out.println("cod : "+datos[c].get("COD")+" descripcion: "+datos[c].get("DESCRIPCION"));
                    JCB_TipoAdj.addItem(datos[c].get("COD_ADJUNTO") + "-" + datos[c].get("DESCRIPCION"));
                }
            }
        } catch (RemoteException e) {
            javax.swing.JOptionPane.showMessageDialog(this, "<html> error de conexion con el servidor <br> " + e, "SYSTEM CAPRICORN",
                    javax.swing.JOptionPane.ERROR_MESSAGE);
        } catch (ServiceException e) {
            System.out.println(e);
        }
    }*/

    private void ConstruyeTablaDocumentos() {
        //cod_rol=5;
        //origen="ADQ";
        documentos = new TablaDocumentos();
        TblDocumentos.setAutoCreateColumnsFromModel(false);
        TblDocumentos.setModel(documentos);

        for (int k = 0; k < TablaDocumentos.m_columns.length; k++) {
            TableCellRenderer renderer = new MiRenderer(TablaDocumentos.m_columns[k].m_alignment);
            TableColumn column = new TableColumn(k, TablaDocumentos.m_columns[k].m_width, renderer, null);
            TblDocumentos.addColumn(column);
        }
        JTableHeader header = TblDocumentos.getTableHeader();
        header.setUpdateTableInRealTime(true);
        header.setReorderingAllowed(true);
        PnlDocumentos.getViewport().add(TblDocumentos);

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        PnlDocumentos = new javax.swing.JScrollPane();
        jScrollPane2 = new javax.swing.JScrollPane();
        TblDocumentos = new javax.swing.JTable();
        BtnDelete = new javax.swing.JButton();
        BtnDescarga = new javax.swing.JButton();
        TxtNombreArchivo = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        BtnExaminar = new javax.swing.JButton();
        TxtDescripcionArchivo = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        BtnAdjuntar = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel2.setBackground(new java.awt.Color(185, 203, 221));
        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel2.setLayout(null);

        TblDocumentos.setAutoCreateColumnsFromModel(false);
        TblDocumentos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(TblDocumentos);

        PnlDocumentos.setViewportView(jScrollPane2);

        jPanel2.add(PnlDocumentos);
        PnlDocumentos.setBounds(20, 60, 840, 130);

        BtnDelete.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
        BtnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/umsa/capricornio/gui/images/eliminar.gif"))); // NOI18N
        BtnDelete.setText("Eliminar");
        BtnDelete.setName(""); // NOI18N
        BtnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnDeleteActionPerformed(evt);
            }
        });
        jPanel2.add(BtnDelete);
        BtnDelete.setBounds(290, 210, 120, 28);

        BtnDescarga.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
        BtnDescarga.setIcon(new javax.swing.ImageIcon(getClass().getResource("/umsa/capricornio/gui/images/drive_disk.png"))); // NOI18N
        BtnDescarga.setText("Ver Documento");
        BtnDescarga.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnDescargaActionPerformed(evt);
            }
        });
        jPanel2.add(BtnDescarga);
        BtnDescarga.setBounds(90, 210, 140, 28);

        TxtNombreArchivo.setEnabled(false);
        TxtNombreArchivo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TxtNombreArchivoActionPerformed(evt);
            }
        });
        jPanel2.add(TxtNombreArchivo);
        TxtNombreArchivo.setBounds(160, 260, 250, 27);

        jLabel18.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel18.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel18.setText("Nombre de Archivo:");
        jPanel2.add(jLabel18);
        jLabel18.setBounds(30, 260, 150, 20);

        BtnExaminar.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
        BtnExaminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/umsa/capricornio/gui/images/magnifier.png"))); // NOI18N
        BtnExaminar.setText("Examinar");
        BtnExaminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnExaminarActionPerformed(evt);
            }
        });
        jPanel2.add(BtnExaminar);
        BtnExaminar.setBounds(490, 210, 120, 28);
        jPanel2.add(TxtDescripcionArchivo);
        TxtDescripcionArchivo.setBounds(570, 260, 250, 27);

        jLabel6.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel6.setText("Descripcion Archivo:");
        jPanel2.add(jLabel6);
        jLabel6.setBounds(440, 260, 130, 20);

        BtnAdjuntar.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
        BtnAdjuntar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/umsa/capricornio/gui/images/plus_16.png"))); // NOI18N
        BtnAdjuntar.setText("Adjuntar");
        BtnAdjuntar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAdjuntarActionPerformed(evt);
            }
        });
        jPanel2.add(BtnAdjuntar);
        BtnAdjuntar.setBounds(690, 210, 120, 28);

        jButton1.setFont(new java.awt.Font("Arial", 1, 13)); // NOI18N
        jButton1.setForeground(new java.awt.Color(255, 0, 0));
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/umsa/capricornio/gui/images/book_previous.png"))); // NOI18N
        jButton1.setText("SALIR");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton1);
        jButton1.setBounds(570, 330, 100, 28);

        jLabel2.setFont(new java.awt.Font("Arial", 1, 13)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 0, 51));
        jLabel2.setText("ADJUNTAR CONTRATOS");
        jPanel2.add(jLabel2);
        jLabel2.setBounds(370, 20, 210, 15);

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/umsa/capricornio/gui/images/action_refresh_blue.gif"))); // NOI18N
        jButton2.setText("Modificar Fechas");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton2);
        jButton2.setBounds(211, 330, 160, 28);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 895, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, 391, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public void AbreDialogo() {
        setVisible(true);
    }
    private void BtnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnDeleteActionPerformed
        // TODO add your handling code here:
        int fila = TblDocumentos.getSelectedRow();
        int res = javax.swing.JOptionPane.showConfirmDialog(this, "¿Desea ELIMINAR EL ARCHIVO ADJUNTO?",
                "MENSAJE CAPRICORNIO", javax.swing.JOptionPane.YES_NO_OPTION);
        if (res != javax.swing.JOptionPane.YES_OPTION) {
            return;
        }
        try {
            AdquiWSServiceLocator servicio = new AdquiWSServiceLocator();
            AdquiWS_PortType puerto = servicio.getAdquiWS();
            Map[] datos = null;
            datos = puerto.setAnulaDocumento("SET-upDateAnulaDocumento", Integer.parseInt(TblDocumentos.getValueAt(fila, 0).toString()));
            javax.swing.JOptionPane.showMessageDialog(this, "EL DOCUMENTO ADJUNTO FUE ANULADO", "SYSTEM CAPRICORN",
                    javax.swing.JOptionPane.INFORMATION_MESSAGE);

        } catch (RemoteException e) {
            javax.swing.JOptionPane.showMessageDialog(this, "<html> error de conexion con el servidor <br> " + e, "SYSTEM CAPRICORN",
                    javax.swing.JOptionPane.ERROR_MESSAGE);
        } catch (ServiceException e) {
            System.out.println(e);
        } catch (IllegalArgumentException e) {
            javax.swing.JOptionPane.showMessageDialog(this, "Debe elegir una fila de la bandeja de salida para enviar el memorandum", "SYSTEM CAPRICORN",
                    javax.swing.JOptionPane.ERROR_MESSAGE);
        }
        LlenaDocumentos();
    }//GEN-LAST:event_BtnDeleteActionPerformed

    public static void openWebpage(String urlString)
    {
        try {
            Desktop.getDesktop().browse(new URL(urlString).toURI());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    
    private void BtnDescargaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnDescargaActionPerformed
        // TODO add your handling code here:
        int fila = TblDocumentos.getSelectedRow();
        String nombre_doc = TblDocumentos.getValueAt(fila, 2).toString();
        String nombre_ruta = TblDocumentos.getValueAt(fila, 3).toString();
        String url = "http://200.7.160.182/prueba/" + nombre_doc; //dirección url del recurso a descargar
        openWebpage(url);
        /*String name = nombre_doc; //nombre del archivo destino
        //Directorio destino para las descargas
        String folder = "c:/descargas/";
        int res = javax.swing.JOptionPane.showConfirmDialog(this, "¿Desea DESCARGAR EL ARCHIVO ADJUNTO?",
                "MENSAJE CAPRICORNIO", javax.swing.JOptionPane.YES_NO_OPTION);
        if (res != javax.swing.JOptionPane.YES_OPTION) {
            return;
        }
        try {
            //Crea el directorio de destino en caso de que no exista
            File dir = new File(folder);

            if (!dir.exists()) {
                if (!dir.mkdir()) {
                    return; // no se pudo crear la carpeta de destino
                }
            }
            File file = new File(folder + name);
            URLConnection conn = new URL(url).openConnection();
            conn.connect();
            System.out.println("\nempezando descarga: \n");
            System.out.println(">> URL: " + url);
            System.out.println(">> Nombre: " + name);
            System.out.println(">> tamaño: " + conn.getContentLength() + " bytes");
            InputStream in = conn.getInputStream();
            OutputStream out = new FileOutputStream(file);

            int b = 0;
            while (b != -1) {
                b = in.read();
                if (b != -1) {
                    out.write(b);
                }
            }
            out.close();
            in.close();
            javax.swing.JOptionPane.showMessageDialog(this, "EL ARCHIVO SELECCIONADO " + nombre_doc + " FUE DESCARGADO EN SU SU COMPUTADORA EN C:/descargas", "SYSTEM CAPRICORN",
                    javax.swing.JOptionPane.INFORMATION_MESSAGE);
        } catch (MalformedURLException e) {
            System.out.println("la url: " + url + " no es valida!");
        } catch (IOException e) {
            e.printStackTrace();
        }*/
    }//GEN-LAST:event_BtnDescargaActionPerformed

    private void TxtNombreArchivoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TxtNombreArchivoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_TxtNombreArchivoActionPerformed

    private void BtnExaminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnExaminarActionPerformed
        // TODO add your handling code here:
        String[] csv = new String[]{"csv", "txt", "jpg", "doc", "xls", "pdf"};
        JFileChooser FileAdjuntaArchivo = new JFileChooser();
        FileAdjuntaArchivo.setAcceptAllFileFilterUsed(false);
        //FileImportaVeneficiarios.addChoosableFileFilter(new SimpleFileFilter(csv,"Datos a IMPORTAR (*.csv, *.txt)"));
        int option = FileAdjuntaArchivo.showOpenDialog(this);
        if (option == JFileChooser.APPROVE_OPTION) {
            rutaArchivo = FileAdjuntaArchivo.getSelectedFile();
            if (FileAdjuntaArchivo.getSelectedFile() != null) {
                lectura = null;
                Archivos archivo = Archivos.getInstance();
                try {
                    lectura = archivo.leerArchivo(rutaArchivo.getAbsolutePath()).trim();
                    TxtNombreArchivo.setText(rutaArchivo.getAbsolutePath());
                    nombre_archivo = rutaArchivo.getName();
                    //MigraDatosTabla("\\|"); // ESTE METODO TAMBIEN FUNCIONA, ambos son validos
                } catch (FileNotFoundException e) {
                    javax.swing.JOptionPane.showMessageDialog(this, "No se encontro el archivo \n" + e, "CAJA PAGADORA",
                            javax.swing.JOptionPane.ERROR_MESSAGE);
                } catch (IOException e) {
                    javax.swing.JOptionPane.showMessageDialog(this, "Error de Flujo Entrada Salida \n" + e, "CAJA PAGADORA",
                            javax.swing.JOptionPane.ERROR_MESSAGE);
                }
            }
            //MigraDatosTabla("|");
        } else {
            System.out.println("Cancelado.");
        }
    }//GEN-LAST:event_BtnExaminarActionPerformed

    private String fechasContrato(int cod_transaccion,int valor,int cod_trans_nro)
    {
      //JTextField xField = new JTextField(5);
      //JTextField texto = new JTextField(60);
      //JTextField field1 = new JTextField(5);
      Date xx=new Date();
      Date yy=new Date();
      DateField xfield=new DateField();
      DateField yfield=new DateField();
      xfield.setDateFormat(DateFormat.getDateInstance(DateFormat.MEDIUM));
      yfield.setDateFormat(DateFormat.getDateInstance(DateFormat.MEDIUM));
      xfield.setValue(xx);
      yfield.setValue(yy);
      JPanel myPanel = new JPanel();
      myPanel.add(new JLabel("Fecha de Inicio:"));
      myPanel.add(xfield);
      myPanel.add(new JLabel("Fecha de Final:"));
      myPanel.add(yfield);
      //myPanel.add(new JLabel("Ingrese los dias:"));
      //myPanel.add(field1);
      /*myPanel.add(new JLabel("INGRESE LA FORMA DE PAGO:"));
      myPanel.add(texto);*/
      myPanel.add(Box.createHorizontalStrut(15)); // a spacer
      /*myPanel.add(new JLabel("y:"));
      myPanel.add(yField);*/
      String convertido,convertido1;
      int result = JOptionPane.showConfirmDialog(null, myPanel, 
               "POR FAVOR INSERTE LAS FECHAS O LOS DIAS EN CASO DE TENER UN ESTIMADO", JOptionPane.OK_CANCEL_OPTION);
      if (result == JOptionPane.OK_OPTION) {
        //System.out.println("x value: " + field1.getText());
        /*if(field1.getText().equals(""))
        {
            System.out.println("siiiiiiiiiiiiiiiiiiiii");
        }
        if(!field1.getText().equals(""))
        {
            System.out.println("aqui aja");
            convertido=field1.getText();
        }*/
        /*else
        {*/
            DateFormat fecha = new SimpleDateFormat("dd/MM/yyyy");
            convertido = fecha.format(xfield.getValue());
            convertido1= fecha.format(yfield.getValue());
            System.out.println(convertido+" - "+convertido1);  
            try{
                AdquiWSServiceLocator servicio = new AdquiWSServiceLocator();
                AdquiWS_PortType puerto = servicio.getAdquiWS();
                System.out.println("la fi="+xfield.getValue()+" la ff="+yfield.getValue());
                System.out.println("la fi="+convertido+" la ff="+convertido1+" _cod"+cod_trans_nro+" asd "+cod_transaccion);
                Map[] datos=puerto.fechasTramite2(convertido,convertido1,cod_transaccion,cod_trans_nro,valor);
                System.out.println("la fi="+convertido+" la ff="+convertido1);
                /*System.out.println("y value: " +datos[0].get("DIAS").toString());
                System.out.println("esto de aqui esta entrando correctamente");
                convertido=datos[0].get("DIAS").toString();*/
                convertido="1";
            }
            catch(Exception e)
            {
                System.out.println("esto es un error: "+e);
            }
        //}
        
      }
      else
      {
        convertido=null;
      }
      /*if(!texto.getText().equals(""))
      {*/
          //convertido=convertido+"#"+"con cheque";
      //}
      //int dias=yfield.getValue()-xfield.getValue();
      return convertido;
    }
    
    private void BtnAdjuntarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAdjuntarActionPerformed
        // TODO add your handling code here:
        /*String cd="1";
        if(cod_tipo_adj==9)
        {
            cd=fechasContrato(cod_transaccion);
        }
        */
        //if(cd!=null)
        //{
            String nombre_nuevo = "";
            String ultimo_valor = "";
            //Obtener el ultimo numero de la transaccion
            try {
                //javax.swing.JOptionPane.showMessageDialog(this, " U1", "SYSTEM CAPRICORN",javax.swing.JOptionPane.INFORMATION_MESSAGE);
                AdquiWSServiceLocator servicio1 = new AdquiWSServiceLocator();
                AdquiWS_PortType puerto1 = servicio1.getAdquiWS();
                Map[] datos1 = puerto1.getUltimoDocumento();
                //javax.swing.JOptionPane.showMessageDialog(this, " U2", "SYSTEM CAPRICORN",javax.swing.JOptionPane.INFORMATION_MESSAGE);
                if (datos1 != null) {
                    //javax.swing.JOptionPane.showMessageDialog(this, " U3", "SYSTEM CAPRICORN",javax.swing.JOptionPane.INFORMATION_MESSAGE);
                    for (int c = 0; c < datos1.length; c++) {
                        //documentos.insert(c);
                        //TblDocumentos.tableChanged(new TableModelEvent(documentos, c, c, TableModelEvent.ALL_COLUMNS,TableModelEvent.INSERT));
                        //TblDocumentos.setValueAt(datos1[c].get("COD_DOCS"),c,0);
                        ultimo_valor = (String) datos1[c].get("ULTIMO");
                    }
                }
            } catch (RemoteException e) {
                javax.swing.JOptionPane.showMessageDialog(this, "<html> error de conexion con el servidor <br> " + e, "SYSTEM CAPRICORN",
                        javax.swing.JOptionPane.ERROR_MESSAGE);
            } catch (ServiceException e) {
                System.out.println(e);
            }
            this.cod_tipo_adj=9;
            //Grabar los datos del documento
            //javax.swing.JOptionPane.showMessageDialog(this, " V1 and cod_tipo_adj= "+this.cod_tipo_adj, "SYSTEM CAPRICORN",javax.swing.JOptionPane.INFORMATION_MESSAGE);
            if (!"".equals(TxtNombreArchivo.getText()) && this.cod_tipo_adj != -1) {
                //this.BtnAdjuntar.setEnabled(true);
                try {
                    //javax.swing.JOptionPane.showMessageDialog(this, " V1", "SYSTEM CAPRICORN",javax.swing.JOptionPane.INFORMATION_MESSAGE);
                    String ext = getExtension(TxtNombreArchivo.getText());
                    nombre_nuevo = "139_" + String.valueOf(cod_transaccion) + "_" + ultimo_valor + "." + ext;
                    if (AdjuntarArchivo(TxtNombreArchivo.getText(), nombre_nuevo)) {
                        //javax.swing.JOptionPane.showMessageDialog(this, " V2", "SYSTEM CAPRICORN",javax.swing.JOptionPane.INFORMATION_MESSAGE);
                        AdquiWSServiceLocator servicio = new AdquiWSServiceLocator();
                        AdquiWS_PortType puerto = servicio.getAdquiWS();
                        Map[] datos = null;
                        String detalle = null;
                        if (!"".equals(TxtNombreArchivo.getText())) {
                            //javax.swing.JOptionPane.showMessageDialog(this, " V3", "SYSTEM CAPRICORN",javax.swing.JOptionPane.INFORMATION_MESSAGE);
                            detalle = TxtNombreArchivo.getText();
                            datos = puerto.setDocumentos("SET-insDataDoc", cod_transaccion, nombre_nuevo, "/opt/tomcat/webapps/prueba", TxtDescripcionArchivo.getText(), "V", this.cod_tipo_adj);
                            javax.swing.JOptionPane.showMessageDialog(this, "ARCHIVO ALMACENADO CORRECTAMENTE", "SYSTEM CAPRICORN",
                                    javax.swing.JOptionPane.INFORMATION_MESSAGE);
                        }
                    }
                    //cerrarDiag();
                } catch (RemoteException e) {
                    javax.swing.JOptionPane.showMessageDialog(this, "<html> error de conexion con el servidor <br> " + e, "SYSTEM CAPRICORN",
                            javax.swing.JOptionPane.ERROR_MESSAGE);
                } catch (ServiceException e) {
                    System.out.println(e);
                }
            } else {
                javax.swing.JOptionPane.showMessageDialog(menu, "No eligio ningun archivo para subir\n o no le asigno un tipo al archivo adjunto", "SYSTEM CAPRICORN",
                        javax.swing.JOptionPane.ERROR_MESSAGE);
            }
            TxtNombreArchivo.setText("");
            TxtDescripcionArchivo.setText("");
            String cd=null;
            while(cd==null)
            {
                cd=fechasContrato(cod_transaccion,Integer.parseInt(ultimo_valor),cod_trans_nro);
            }
            LlenaDocumentos();
        //}
        
    }//GEN-LAST:event_BtnAdjuntarActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        //this.
        System.gc();
        r = Runtime.getRuntime();
        long mem1 = r.freeMemory();
        dispose();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        int fila = TblDocumentos.getSelectedRow();
        int res = javax.swing.JOptionPane.showConfirmDialog(this, "¿Desea MODIFICAR LAS FECHAS DE CONTRATO?",
                "MENSAJE CAPRICORNIO", javax.swing.JOptionPane.YES_NO_OPTION);
        if (res != javax.swing.JOptionPane.YES_OPTION) {
            return;
        }
        try {
            /*AdquiWSServiceLocator servicio = new AdquiWSServiceLocator();
            AdquiWS_PortType puerto = servicio.getAdquiWS();
            Map[] datos = null;
            datos = puerto.setAnulaDocumento("SET-upDateAnulaDocumento", Integer.parseInt(TblDocumentos.getValueAt(fila, 0).toString()));
            javax.swing.JOptionPane.showMessageDialog(this, "EL DOCUMENTO ADJUNTO FUE ANULADO", "SYSTEM CAPRICORN",
                    javax.swing.JOptionPane.INFORMATION_MESSAGE);*/
            int a=Integer.parseInt(TblDocumentos.getValueAt(fila, 0).toString());
            String cd=fechasContrato(cod_transaccion, a, cod_trans_nro);
        } catch (IllegalArgumentException e) {
            javax.swing.JOptionPane.showMessageDialog(this, "Debe elegir una fila de la bandeja de salida para enviar el memorandum", "SYSTEM CAPRICORN",
                    javax.swing.JOptionPane.ERROR_MESSAGE);
        }
        LlenaDocumentos();
    }//GEN-LAST:event_jButton2ActionPerformed

    private String getTipoAdj(String cadena) {
        int i = -1;
        String aux = null;
        for (int j = 0; j < cadena.length(); j++) {
            //cadena.charAt(j);
            if (cadena.charAt(j) == '-') {
                i = j;
                break;
            }
        }
        //System.out.println("El valor de i --> "+i);
        if (i != -1) {
            aux = cadena.substring(0, i);
        }
        return aux;
    }
    boolean AdjuntarArchivo(String ruta_archivo, String nombre_archivo) {
        boolean sw = false;
        HessianProxyFactory proxy = new HessianProxyFactory();
        try {
            //call proxy for Upload
            HelloService x = (HelloService) proxy.create(HelloService.class, "http://200.7.160.182/HessianServerI/HelloServlet");
            InputStream in;
            try {
                in = new FileInputStream(ruta_archivo);
                x.upload("/opt/tomcat/webapps/prueba/" + nombre_archivo, in);
                sw = true;
            } catch (FileNotFoundException ex) {
                sw = false;
                //Logger.getLogger(HessianFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (MalformedURLException ex) {
            sw = false;
            //Logger.getLogger(HessianFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sw;
    }

    private void LlenaDocumentos() {
        try {
            AdquiWSServiceLocator servicio = new AdquiWSServiceLocator();
            AdquiWS_PortType puerto = servicio.getAdquiWS();
            Map[] datos = puerto.getDocumentos2(cod_transaccion,cod_trans_nro);
            CerearTablaDocumentos();
            if (datos != null) {
                for (int c = 0; c < datos.length; c++) {
                    documentos.insert(c);
                    TblDocumentos.tableChanged(new TableModelEvent(documentos, c, c, TableModelEvent.ALL_COLUMNS, TableModelEvent.INSERT));
                    TblDocumentos.setValueAt(datos[c].get("COD_DOCS"), c, 0);
                    TblDocumentos.setValueAt(datos[c].get("COD_TRANSACCION"), c, 1);
                    TblDocumentos.setValueAt(datos[c].get("TERMINOS_REF"), c, 2);
                    TblDocumentos.setValueAt(datos[c].get("UBICACION"), c, 3);
                    TblDocumentos.setValueAt(datos[c].get("DESCRIPCION"), c, 4);
                    TblDocumentos.setValueAt(datos[c].get("FECHA_INI_CON"), c, 5);
                    TblDocumentos.setValueAt(datos[c].get("FECHA_FIN_CON"), c, 6);

                }
            }
        } catch (RemoteException e) {
            javax.swing.JOptionPane.showMessageDialog(this, "<html> error de conexion con el servidor <br> " + e, "SYSTEM CAPRICORN",
                    javax.swing.JOptionPane.ERROR_MESSAGE);
        } catch (ServiceException e) {
            System.out.println(e);
        }
    }

    public static String getExtension(String filename) {
        int index = filename.lastIndexOf('.');
        if (index == -1) {
            return "";
        } else {
            return filename.substring(index + 1);
        }
    }

    void CerearTablaDocumentos() {
        int f = TblDocumentos.getRowCount();
        for (int i = f - 1; i >= 0; i--) {
            if (documentos.delete(i)) {
                TblDocumentos.tableChanged(new TableModelEvent(
                        documentos, i, i, TableModelEvent.ALL_COLUMNS, TableModelEvent.INSERT));
            }
        }
    }
    /**
     * @param args the command line arguments
     */
    /*public static void main(String args[]) {
       
     java.awt.EventQueue.invokeLater(new Runnable() {
     public void run() {
     JDAdjuntos dialog = new JDAdjuntos(new javax.swing.JFrame(), true);
     dialog.addWindowListener(new java.awt.event.WindowAdapter() {
     @Override
     public void windowClosing(java.awt.event.WindowEvent e) {
     System.exit(0);
     }
     });
     dialog.setVisible(true);
     }
     });
     }*/

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtnAdjuntar;
    private javax.swing.JButton BtnDelete;
    private javax.swing.JButton BtnDescarga;
    private javax.swing.JButton BtnExaminar;
    private javax.swing.JScrollPane PnlDocumentos;
    private javax.swing.JTable TblDocumentos;
    private javax.swing.JTextField TxtDescripcionArchivo;
    private javax.swing.JTextField TxtNombreArchivo;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane2;
    // End of variables declaration//GEN-END:variables
}
