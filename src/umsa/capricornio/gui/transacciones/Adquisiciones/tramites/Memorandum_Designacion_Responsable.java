/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package umsa.capricornio.gui.transacciones.Adquisiciones.tramites;

import java.awt.Color;
import java.awt.event.ItemEvent;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.StringTokenizer;
import static javax.print.attribute.Size2DSyntax.MM;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import umsa.capricornio.gui.ConnectADQUI.AdquiWSServiceLocator;
import umsa.capricornio.gui.ConnectADQUI.AdquiWS_PortType;
import umsa.capricornio.gui.transacciones.reporte.GetResAdj;
import umsa.capricornio.gui.transacciones.reporte.GetResoluciones;

/**
 *
 * @author alex
 */
public class Memorandum_Designacion_Responsable extends javax.swing.JFrame {
    
    private int cod_transaccion, gestion, cod_w, cod_almacen, cod_usuario, cod_trans_nro, cod_rol, cod_res_ini, num_desiertos;

    /**
     * Creates new form Memorandum_Designacion_Responsable
     */
    public Memorandum_Designacion_Responsable() {
        initComponents();
    }
    //cod_transaccion,gestion,cod_w,cod_almacen,cod_usuario,cod_trans_nro,cod_rol,cod_res_ini,num_desiertos

    public Memorandum_Designacion_Responsable(int cod_transaccion, int gestion, int cod_w, int cod_almacen, int cod_usuario, int cod_trans_nro, int cod_rol, int cod_res_ini, int num_desiertos) {
        initComponents();
        this.setBackground(Color.BLUE);
        this.cod_transaccion = cod_transaccion;
        this.gestion = gestion;
        this.cod_w = cod_w;
        this.cod_almacen = cod_almacen;
        this.cod_usuario = cod_usuario;
        this.cod_trans_nro = cod_trans_nro;
        this.cod_rol = cod_rol;
        this.cod_res_ini = cod_res_ini;
        this.num_desiertos = num_desiertos;
        llenarDatos();
        // j
    }
    
    public void llenarDatos() {
        /*OJO  JAVIER*/
        try {
            AdquiWSServiceLocator servicio = new AdquiWSServiceLocator();
            AdquiWS_PortType puerto = servicio.getAdquiWS();
            //fila : cod_tipo_res
            //puerto.getrestipo(cod_w, cod_rol, fila);
            Map[] datos = puerto.getrestipo(this.cod_transaccion, this.cod_trans_nro, 8);
            System.out.println("ENTRADA X1");
            /*if(datos!=null){
            }*/
            // int tamanio = datos.length;
            System.out.println("SALIDA X1");

            //System.out.println("VALORES DE LOS REGISTYRO DE CANTYIDAD : " + tamanio);
            /* String aaa1 = datos[0].get("FECHA").toString();
            int app = Integer.parseInt(datos[0].get("GESTION").toString());
            
            this.LlenaItems();*/
            if (datos != null) {
                String responsable = datos[0].get("RESPONSABLE").toString();
                String nombre_cargo = datos[0].get("NOMBRE_CARGO").toString();
                String fecha_propuesta = datos[0].get("FECHA_PROPUESTA").toString();
                String oficina_propuesta = datos[0].get("OFICINA_PROPUESTA").toString();
                String oficina_apertura = datos[0].get("OFICINA_APERTURA").toString();
                String hora_apertura = datos[0].get("HORA_APERTURA").toString();
                String fecha_calificacion = datos[0].get("FECHA_CALIFICACION").toString();
                String cuce_sicoes = datos[0].get("CUCE_SICOES").toString();
                
                System.out.println("fecha propuesta : " + fecha_propuesta);
                
                System.out.println("fecha calificacion : " + fecha_calificacion);
                
                StringTokenizer fech = new StringTokenizer(fecha_propuesta, "/");
                String mes = fech.nextToken();
                String dis = fech.nextToken();
                String anio = fech.nextToken();
                String fecha_propuesta1 = dis + "/" + mes + "/" + anio;
                System.out.println("FECHA  propuesta FINAL : " + fecha_calificacion);
                
                StringTokenizer fech1 = new StringTokenizer(fecha_calificacion, "/");
                String mes1 = fech1.nextToken();
                String dis1 = fech1.nextToken();
                String anio1 = fech1.nextToken();
                String fecha_calificacion1 = dis1 + "/" + mes1 + "/" + anio1;
                System.out.println("FECHA  calificacion FINAL : " + fecha_calificacion1);
                
                if (responsable.equals("COMISIÓN DE CALIFICACIÓN")) {
                    radioComision.setSelected(true);
                }
                if (responsable.equals("RESPONSABLE DE CALIFICACIÓN")) {
                    radioResponsable.setSelected(true);
                }
                
                txtNombre.setText(nombre_cargo);
                
                SimpleDateFormat formatoDeFecha = new SimpleDateFormat("dd/MM/yyyy");
                Date date = formatoDeFecha.parse(fecha_propuesta1);
                Date date1 = formatoDeFecha.parse(fecha_calificacion1);
                
                this.fecha_propuesta.setValue(date);
                this.fecha_calificacion.setValue(date1);
                
                txtOficinaApertura.setText(oficina_apertura);
                txtOficinaPropuesta.setText(oficina_propuesta);
                txtHoraApertura.setText(hora_apertura);
                txtCuce_Sicoes.setText(cuce_sicoes);
                
            } else {
                System.out.println("ESTROY AQUI EN 000000000f");
            }
            
        } catch (Exception e) {
            // JOptionPane.showMessageDialog(this, "Debe seleccionar un item", "Error", JOptionPane.ERROR_MESSAGE);
            System.out.println("ERROR EN EJECUCION AQUI rrrr");
        }
        /*OJO  JAVIER*/
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        fecha_propuesta = new net.sf.nachocalendar.components.DateField();
        txtOficinaPropuesta = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtOficinaApertura = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txtHoraApertura = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        fecha_calificacion = new net.sf.nachocalendar.components.DateField();
        radioComision = new javax.swing.JRadioButton();
        radioResponsable = new javax.swing.JRadioButton();
        btnGenerar = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtNombre = new javax.swing.JTextArea();
        btnModificar = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        txtCuce_Sicoes = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setBackground(new java.awt.Color(0, 153, 153));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel1.setText("RESPONSABLE");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel2.setText("NOMBRES Y CARGOS");

        jLabel3.setBackground(new java.awt.Color(185, 203, 221));
        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel3.setText("FECHA DE PROPUESTA");

        fecha_propuesta.setDateFormat(DateFormat.getDateInstance(DateFormat.MEDIUM));
        fecha_propuesta.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel4.setText("OFICINA DE PROPUESTA");

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel5.setText("OFICINA DE APERTURA");

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel6.setText("HORA DE APERTURA");

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel7.setText("FECHA DE CALIFICACION");

        fecha_calificacion.setDateFormat(DateFormat.getDateInstance(DateFormat.MEDIUM));
        fecha_calificacion.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N

        radioComision.setText("COMISIÓN DE CALIFICACIÓN");
        radioComision.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioComisionActionPerformed(evt);
            }
        });

        radioResponsable.setText("RESPONSABLE DE CALIFICACIÓN");
        radioResponsable.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioResponsableActionPerformed(evt);
            }
        });

        btnGenerar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/umsa/capricornio/gui/images/accept (2).png"))); // NOI18N
        btnGenerar.setText("GENERAR");
        btnGenerar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGenerarActionPerformed(evt);
            }
        });

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/umsa/capricornio/gui/images/doc_table.png"))); // NOI18N
        jButton2.setText("IMPRIMIR");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/umsa/capricornio/gui/images/cancel.png"))); // NOI18N
        jButton1.setText("SALIR");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("MS PGothic", 1, 12)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(0, 102, 255));
        jLabel8.setText("MEMORANDUM DE DESIGNACION RESPONSABLE");

        txtNombre.setColumns(20);
        txtNombre.setRows(5);
        jScrollPane1.setViewportView(txtNombre);

        btnModificar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/umsa/capricornio/gui/images/disk (2).png"))); // NOI18N
        btnModificar.setText("MODIFICAR");
        btnModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificarActionPerformed(evt);
            }
        });

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel9.setText("CUCE SICOES");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(41, 41, 41)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(btnGenerar)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel6)
                                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel5)
                                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel7)
                                .addComponent(jLabel9)))
                        .addGap(38, 38, 38)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnModificar)
                        .addGap(30, 30, 30)
                        .addComponent(jButton1)
                        .addGap(18, 18, 18)
                        .addComponent(jButton2)
                        .addGap(0, 18, Short.MAX_VALUE))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(txtOficinaApertura)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(radioComision, javax.swing.GroupLayout.PREFERRED_SIZE, 261, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(radioResponsable, javax.swing.GroupLayout.DEFAULT_SIZE, 283, Short.MAX_VALUE)
                                .addComponent(jScrollPane1))
                            .addComponent(fecha_propuesta, javax.swing.GroupLayout.PREFERRED_SIZE, 263, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(txtHoraApertura)
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(fecha_calificacion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(txtOficinaPropuesta, javax.swing.GroupLayout.Alignment.TRAILING))
                            .addGap(20, 20, 20))
                        .addComponent(txtCuce_Sicoes, javax.swing.GroupLayout.PREFERRED_SIZE, 263, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(19, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 346, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(69, 69, 69))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(radioComision, javax.swing.GroupLayout.DEFAULT_SIZE, 33, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(radioResponsable)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 34, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(fecha_propuesta, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtOficinaPropuesta, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(13, 13, 13)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel5)
                    .addComponent(txtOficinaApertura, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(26, 26, 26)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtHoraApertura, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(fecha_calificacion, javax.swing.GroupLayout.DEFAULT_SIZE, 24, Short.MAX_VALUE))
                .addGap(51, 51, 51)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(txtCuce_Sicoes, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 31, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton2)
                    .addComponent(jButton1)
                    .addComponent(btnModificar)
                    .addComponent(btnGenerar))
                .addGap(58, 58, 58))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void radioComisionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioComisionActionPerformed
        // TODO add your handling code here:

        radioResponsable.setSelected(false);
    }//GEN-LAST:event_radioComisionActionPerformed

    private void btnGenerarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGenerarActionPerformed
        // TODO add your handling code here:
        try {
            AdquiWSServiceLocator servicio = new AdquiWSServiceLocator();
            AdquiWS_PortType puerto = servicio.getAdquiWS();
            //fila : cod_tipo_res
            //puerto.getrestipo(cod_w, cod_rol, fila);
            Map[] datos = puerto.getrestipo(this.cod_transaccion, this.cod_trans_nro, 8);
            if (datos == null) {
                insertarDatos();
            } else {
                javax.swing.JOptionPane.showMessageDialog(this, "USTED YA GENERO SU MEMORANDUM DE  DESIGNACION ,\n LO QUE DEBE HACER ES MODIFICAR SU MEMORANDUM", "SYSTEM CAPRICORN",
                        javax.swing.JOptionPane.INFORMATION_MESSAGE);
            }
            
        } catch (Exception e) {
            
            System.out.println("ERROR EN ESTE PUBNTO");
        }
        

    }//GEN-LAST:event_btnGenerarActionPerformed
    public void insertarDatos() {
        String responsable = "";
        if (radioResponsable.isSelected()) {
            responsable = radioResponsable.getText().toString();
            
        }
        if (radioComision.isSelected()) {
            responsable = radioComision.getText().toString();
            
        }
        String nombres = txtNombre.getText().toString();
        String cuce_sicoes = txtCuce_Sicoes.getText().toString();
        Date fp, fc;
        fp = (Date) fecha_propuesta.getValue();
        String oficinaPropuesta = txtOficinaPropuesta.getText().toString();
        String oficinaApertura = txtOficinaApertura.getText().toString();
        String HoraApertura = txtHoraApertura.getText().toString();
        fc = (Date) fecha_calificacion.getValue();
        Calendar calendar = Calendar.getInstance();
        Calendar calendar1 = Calendar.getInstance();
        
        calendar.setTime(fp);
        calendar1.setTime(fc);
        
        String fecha_propuesta1 = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)) + "/" + String.valueOf(calendar.get(Calendar.MONTH) + 1) + "/" + String.valueOf(calendar.get(Calendar.YEAR));
        String fecha_calificacion1 = String.valueOf(calendar1.get(Calendar.DAY_OF_MONTH)) + "/" + String.valueOf(calendar1.get(Calendar.MONTH) + 1) + "/" + String.valueOf(calendar1.get(Calendar.YEAR));
        
        System.out.println("fp en generar : " + fecha_propuesta1);
        System.out.println("fc en generar : " + fecha_calificacion1);
        
        try {
            AdquiWSServiceLocator servicio = new AdquiWSServiceLocator();
            AdquiWS_PortType puerto = servicio.getAdquiWS();
            // System.err.println("cod_transaccion: " + cod_transaccion + ", 1, detalle: " + detalle + ", envia:" + envia + ", dns:" + dns + " , dnp: " + dnp + " , destino: " + destino + " , dias: " + dias + " , gestion: " + gestion + " ,jefe_adqui: " + jefe_adqui + " ,detalle_recursos:" + detalle_recursos + " ,detalle_uni_sol: " + detalle_uni_sol + ",detalle_autorizacion: " + detalle_autorizacion);
            puerto.generaMemorandumDesignacion(8, cod_transaccion, cod_trans_nro, gestion, responsable, nombres, fecha_propuesta1, fecha_calificacion1, oficinaPropuesta, oficinaApertura, HoraApertura, cuce_sicoes);
            
            javax.swing.JOptionPane.showMessageDialog(this, "MEMORANDUM DE DESIGNACION", "SYSTEM CAPRICORN",
                    javax.swing.JOptionPane.INFORMATION_MESSAGE);
            
            System.out.println("Exito :D");
            
        } catch (Exception e) {
            System.err.println("Error :(");
        }
    }
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:

        this.setVisible(false);
        

    }//GEN-LAST:event_jButton1ActionPerformed
    GetResAdj genera_res_adj = new GetResAdj();
    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        genera_res_adj.reporteMemoDesignacion(cod_transaccion, cod_trans_nro);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void radioResponsableActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioResponsableActionPerformed
        // TODO add your handling code here:
        radioComision.setSelected(false);
    }//GEN-LAST:event_radioResponsableActionPerformed

    private void btnModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificarActionPerformed
        // TODO add your handling code here:
        String responsable = "";
        if (radioResponsable.isSelected()) {
            responsable = radioResponsable.getText().toString();
            
        }
        if (radioComision.isSelected()) {
            responsable = radioComision.getText().toString();
            
        }
        String nombres = txtNombre.getText().toString();
        Date fp, fc;
        fp = (Date) fecha_propuesta.getValue();
        String oficinaPropuesta = txtOficinaPropuesta.getText().toString();
        String oficinaApertura = txtOficinaApertura.getText().toString();
        String HoraApertura = txtHoraApertura.getText().toString();
        fc = (Date) fecha_calificacion.getValue();
        Calendar calendar = Calendar.getInstance();
        Calendar calendar1 = Calendar.getInstance();
        
        String cuce_sicoes = txtCuce_Sicoes.getText().toString();
        
        calendar.setTime(fp);
        calendar1.setTime(fc);
        
        String fecha_propuesta1 = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)) + "/" + String.valueOf(calendar.get(Calendar.MONTH) + 1) + "/" + String.valueOf(calendar.get(Calendar.YEAR));
        String fecha_calificacion1 = String.valueOf(calendar1.get(Calendar.DAY_OF_MONTH)) + "/" + String.valueOf(calendar1.get(Calendar.MONTH) + 1) + "/" + String.valueOf(calendar1.get(Calendar.YEAR));
        
        System.out.println("fp en generar : " + fecha_propuesta1);
        System.out.println("fc en generar : " + fecha_calificacion1);
        try {
            AdquiWSServiceLocator servicio = new AdquiWSServiceLocator();
            AdquiWS_PortType puerto = servicio.getAdquiWS();
            // System.err.println("cod_transaccion: " + cod_transaccion + ", 1, detalle: " + detalle + ", envia:" + envia + ", dns:" + dns + " , dnp: " + dnp + " , destino: " + destino + " , dias: " + dias + " , gestion: " + gestion + " ,jefe_adqui: " + jefe_adqui + " ,detalle_recursos:" + detalle_recursos + " ,detalle_uni_sol: " + detalle_uni_sol + ",detalle_autorizacion: " + detalle_autorizacion);
            puerto.setMemorandumDesignacion(cod_transaccion, cod_trans_nro, responsable, nombres, fecha_propuesta1, fecha_calificacion1, oficinaPropuesta, oficinaApertura, HoraApertura, cuce_sicoes);
            
            javax.swing.JOptionPane.showMessageDialog(this, "MODIFICACION REALIZADA AL MEMORANDUM DE DESIGNACION", "SYSTEM CAPRICORN",
                    javax.swing.JOptionPane.INFORMATION_MESSAGE);
            
            System.out.println("Exito :D");
            
        } catch (Exception e) {
            System.err.println("Error :(");
        }
        
    }//GEN-LAST:event_btnModificarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Memorandum_Designacion_Responsable.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Memorandum_Designacion_Responsable.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Memorandum_Designacion_Responsable.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Memorandum_Designacion_Responsable.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Memorandum_Designacion_Responsable().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnGenerar;
    private javax.swing.JButton btnModificar;
    private net.sf.nachocalendar.components.DateField fecha_calificacion;
    private net.sf.nachocalendar.components.DateField fecha_propuesta;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JRadioButton radioComision;
    private javax.swing.JRadioButton radioResponsable;
    private javax.swing.JTextField txtCuce_Sicoes;
    private javax.swing.JTextField txtHoraApertura;
    private javax.swing.JTextArea txtNombre;
    private javax.swing.JTextField txtOficinaApertura;
    private javax.swing.JTextField txtOficinaPropuesta;
    // End of variables declaration//GEN-END:variables
}
