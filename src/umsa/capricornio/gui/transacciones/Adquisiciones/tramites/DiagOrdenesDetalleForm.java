/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package umsa.capricornio.gui.transacciones.Adquisiciones.tramites;

import java.util.Map;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import umsa.capricornio.gui.ConnectADQUI.AdquiWSServiceLocator;
import umsa.capricornio.gui.ConnectADQUI.AdquiWS_PortType;
import umsa.capricornio.gui.menu.FrmMenu;
import umsa.capricornio.gui.transacciones.reporte.GetResAdj;

/**
 *
 * @author alex
 */
public class DiagOrdenesDetalleForm extends javax.swing.JDialog {

    /**
     * Creates new form DiagOrdenesDetalleFrom
     */
    FrmMenu menu;
    JInternalFrame ft;
    private double Total;
    int cod_almacen, cod_trans_nro, cod_rol, gestion, cod_w, tab_habil=0,cod_user;
    private Runtime r;
    String tramite, origen, detalle, unidad_sol, unidad_des, nro, cuantia, del, hasta;
    private boolean sw,sw_modificacion_concluido=false;
    GetResAdj repo = new GetResAdj();
    private int cod_transaccion;
    private String des,dias;
    public DiagOrdenesDetalleForm(JInternalFrame frmt, FrmMenu menu, int cod_almacen, int cod_trans_nro, int cod_rol, String tramite, int gestion, int cod_w, String origen, String detalle, String unidad_sol, String unidad_des, String nro, String cuantia, String del, String hasta,int cod_user,boolean sw_modificacion_concluido,int cod_transaccion) {
        super(menu, false);
        initComponents();
        this.setLocationRelativeTo(null);
        ft = frmt;
        this.menu = menu;
        this.cod_almacen = cod_almacen;
        this.cod_trans_nro = cod_trans_nro;
        this.cod_rol = cod_rol;
        this.tramite = tramite;
        this.gestion = gestion;
        this.cod_w = cod_w;
        this.origen = origen;
        this.detalle = detalle;
        this.unidad_des = unidad_des;
        this.unidad_sol = unidad_sol;
        this.nro = nro;
        this.cuantia = cuantia;
        this.del = del;
        this.hasta = hasta;
        this.cod_user = cod_user;
        this.sw_modificacion_concluido=sw_modificacion_concluido;
        this.cod_transaccion=cod_transaccion;
        //llena_campos();
    }

    
    public void AbreDialogo() {
//        this.sw_modificacion_concluido = sw;
        ft.setVisible(true);
        setVisible(true);
    }
    
    
    public int verifica_contratos_resoluciones()
    {
        try{
            AdquiWSServiceLocator servicio = new AdquiWSServiceLocator();
            AdquiWS_PortType puerto = servicio.getAdquiWS();
            Map[] datos=puerto.getContrato2(cod_transaccion);
            if(datos==null)
            {
                javax.swing.JOptionPane.showMessageDialog(this,"<html> ESTE TRAMITE NO TIENE GENERADO SU CONTRATO POR SISTEMA<br> ","SYSTEM CAPRICORN",
                        javax.swing.JOptionPane.ERROR_MESSAGE);
                return 1;
            }
            else
            {
                Map[] d=puerto.getresmay(cod_transaccion,cod_trans_nro);
                if(d==null)
                {
                    javax.swing.JOptionPane.showMessageDialog(this,"<html> ESTE TRAMITE NO TIENE GENERADO SU RESOLUCION POR SISTEMA<br> ","SYSTEM CAPRICORN",
                        javax.swing.JOptionPane.ERROR_MESSAGE);
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
        }catch(Exception e)
        {
            System.err.println("este error de la verificacion de contratos y resoluciones");
            return 0;
        }
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("OBSERVACIONES Y CIERRE");

        jLabel1.setText("CIERRE Y MODIFICACIONES REALIZADAS EN EL SICOES");

        jButton1.setText("CONCLUIR EL PROCESO");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/umsa/capricornio/gui/images/book_previous.png"))); // NOI18N
        jButton2.setText("SALIR");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton4.setText("ADJUNTAR ARCHIVOS");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addComponent(jButton4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton2)
                .addGap(0, 36, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(107, 107, 107))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jButton2)
                    .addComponent(jButton4))
                .addContainerGap(16, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        try{
            AdquiWSServiceLocator servicio = new AdquiWSServiceLocator();
            AdquiWS_PortType puerto = servicio.getAdquiWS();
            System.out.println(cod_trans_nro+" siiiiiiiiiii");
            Boolean sw_verAdj;
            //Map[] datos=puerto.modificar_formu(jTextField1.getText().toString(),jTextField2.getText().toString(),jTextField3.getText().toString(),jTextField4.getText().toString(),jTextField7.getText().toString(),cod_trans_nro);
            Map[] datos=puerto.getAdjunto(cod_transaccion, 6);
            if (datos!=null){
                System.out.println("Tiene preventivo");
                sw_verAdj = true;
                Map[] d=puerto.cambioEnvio(cod_trans_nro);
                for(int i=0;i<d.length;i++)
                {
                    int x=Integer.parseInt(d[i].get("COD_TRANS_DETALLE").toString());

                    puerto.cambioEnvio1(x, "C");
                }
                this.setVisible(false);
            }
            else{
                System.out.println("No tiene preventivo");
                sw_verAdj = false;
                javax.swing.JOptionPane.showMessageDialog(this,"Debe subir la factura y si es necesario los formularios del sicoes a sistema","SYSTEM CAPRICORN",
                     javax.swing.JOptionPane.ERROR_MESSAGE);
                  return;
            }
            //Map[] datos=puerto.genera
        }catch(Exception e)
        {
            System.err.println("error al almacenar los formularios");
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        this.setVisible(false);
        //ft.setVisible(true);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:
        archivos_adjuntos();
    }//GEN-LAST:event_jButton4ActionPerformed

    
    public void archivos_adjuntos()
    {
        if (this.cod_transaccion != 0) {
            JDAdjuntos JDA = new JDAdjuntos(menu, true, cod_transaccion, cod_rol);
            JDA.AbreDialogo();
        } else {
            JOptionPane.showMessageDialog(null, "Usted no a seleccionado ninguna Solicitud", "Error", JOptionPane.ERROR_MESSAGE);
        }
        //archivos_adjuntos();
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton4;
    private javax.swing.JLabel jLabel1;
    // End of variables declaration//GEN-END:variables
}
